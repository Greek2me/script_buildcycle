// +--------------------------------+
// |  _  _   _   _   _      _     _ |
// | | \/ | |_| |_| |  |_| |  |  |_ |
// | |    | | | |   |_  |  |_ |_ |_ |
// |  Greek2me | Blockland ID 11902 |
// +-----------------------------+--+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

if(forceRequiredAddon("Gamemode_Slayer") == $Error::Addon_NotFound)
{
	error("ERROR: Script_BuildCycle - Required add-on Gamemode_Slayer not found!");
	return;
}

$BuildCycle::Server::Version = "1.0 Alpha 1";

if(!$BuildCycle::Server::registeredVersion)
{
	$Slayer::Server::ModVersions = setField
	(
		$Slayer::Server::ModVersions,
		getFieldCount($Slayer::Server::ModVersions),
		"BuildCycle" SPC $BuildCycle::Server::Version
	);

	$BuildCycle::Server::registeredVersion = 1;
}

// +------------+
// | BuildCycle |
// +------------+
function BuildCycle::onAdd(%this)
{
	if(!isObject(BuildCycle_saveFileHandler))
	{
		$BuildCycle::Server::saveFileHandler = new scriptGroup(BuildCycle_saveFileHandler)
		{
			numSaves = 0;
		};
		if(isObject(missionCleanup) && missionCleanup.isMember(BuildCycle_saveFileHandler))
			missionCleanup.remove(BuildCycle_saveFileHandler);
		%this.add(BuildCycle_saveFileHandler);
	}

	if(!isObject(BuildCycle_minigameConfigFileHandler))
	{
		$BuildCycle::Server::minigameConfigFileHandler = new scriptGroup(BuildCycle_minigameConfigFileHandler)
		{
			numConfigFiles = 0;
		};
		if(isObject(missionCleanup) && missionCleanup.isMember(BuildCycle_minigameConfigFileHandler))
			missionCleanup.remove(BuildCycle_minigameConfigFileHandler);
		%this.add(BuildCycle_minigameConfigFileHandler);
	}

	if(!isObject(BuildCycle_RotationHandler))
	{
		if(isFile("config/server/Build Cycle/buildCycle.cs"))
			exec("config/server/Build Cycle/buildCycle.cs");
		else
			new scriptGroup(BuildCycle_RotationHandler);

		$BuildCycle::Server::RotationHandler = BuildCycle_RotationHandler;

		if(isObject(missionCleanup) && missionCleanup.isMember(BuildCycle_RotationHandler))
			missionCleanup.remove(BuildCycle_RotationHandler);
		%this.add(BuildCycle_RotationHandler);
	}
}

function BuildCycle::canEdit(%this,%client)
{
	return %client.isHost;
}

function BuildCycle::sendInitialData(%this,%client)
{
	Slayer_Support::Debug(0,"Build Cycle: Sending Initial Data",%client.getSimpleName());

	//START THE INITIAL DATA TRANSFER
	commandToClient(%client,'BC_getInitialData_Start');

	BuildCycle_saveFileHandler.sendSaveData(%client);
	BuildCycle_minigameConfigFileHandler.sendConfigData(%client);

	//END THE INITIAL DATA TRANSFER
	commandToClient(%client,'BC_getInitialData_End');
}

function BuildCycle::sendData(%this,%client)
{
	Slayer_Support::Debug(0,"Build Cycle: Sending Data",%client.getSimpleName());

	//START THE DATA TRANSFER
	commandToClient(%client,'BC_getData_Start');

	Slayer.Prefs.sendPrefs(%client,0,"Build Cycle");
	BuildCycle_RotationHandler.sendBuilds(%client);

	%gui = "buildCycle_Main";
	//END THE DATA TRANSFER
	commandToClient(%client,'BC_getData_End',%gui);
}

// +----------------------------+
// | BuildCycle_saveFileHandler |
// +----------------------------+
function BuildCycle_saveFileHandler::onAdd(%this)
{
	//generate all data about save files
	//for use in sending to client/changing map
	if(!%this.saveDataCreated)
		%this.generateSaveData();
}

function BuildCycle_saveFileHandler::generateSaveData(%this) //generate a list of maps and saves that can be sent to clients
{
	//this function used to sort all saves by savename into map lists
	//v21 made this much simpler...

	%this.clearSaveData();

	%path = "saves/*.bls";
	%fileCount = getFileCount(%path);
	for(%i=0; %i < %fileCount; %i++)
	{
		%file = findNextFile(%path);

		%this.saveFile[%this.numSaves] = %file;
		%this.numSaves ++;
	}

	%this.saveDataCreated = 1;
}

function BuildCycle_saveFileHandler::clearSaveData(%this)
{
	for(%i=0; %i < %this.numSaves; %i++)
		%this.saveFile[%i] = "";
	%this.numSaves = 0;
}

function BuildCycle_saveFileHandler::sendSaveData(%this,%client)
{
	//saveFile TRANSFER START
	commandToClient(%client,'BC_getSaveData_Start');

	//saveFile TRANSFER TICK
	for(%i=0; %i < %this.numSaves; %i++)
		commandToClient(%client,'BC_getSaveData_Tick',%this.saveFile[%i]);

	//saveFile TRANSFER END
	commandToClient(%client,'BC_getSaveData_End');
}

// +--------------------------------------+
// | BuildCycle_minigameConfigFileHandler |
// +--------------------------------------+
function BuildCycle_minigameConfigFileHandler::onAdd(%this)
{
	//generate all data about config files
	//for use in sending to client/changing build
	if(!%this.configDataCreated)
		%this.generateConfigData();
}

function BuildCycle_minigameConfigFileHandler::generateConfigData(%this) //generate a list of maps and saves that can be sent to clients
{
	//this function used to sort all saves by savename into map lists
	//v21 made this much simpler...

	%this.clearConfigData();

	%path = $Slayer::Server::ConfigDir @ "/config_saved/config_*.cs";
	%fileCount = getFileCount(%path);
	for(%i=0; %i < %fileCount; %i++)
	{
		%file = findNextFile(%path);

		%this.configFile[%this.numConfigFiles] = %file;
		%this.numConfigFiles ++;
	}

	%this.configDataCreated = 1;
}

function BuildCycle_minigameConfigFileHandler::clearConfigData(%this)
{
	for(%i=0; %i < %this.numConfigFiles; %i++)
		%this.configFile[%i] = "";
	%this.numConfigFiles = 0;
}

function BuildCycle_minigameConfigFileHandler::sendConfigData(%this,%client)
{
	//saveFile TRANSFER START
	commandToClient(%client,'BC_getMinigameConfigData_Start');

	//saveFile TRANSFER TICK
	for(%i=0; %i < %this.numConfigFiles; %i++)
		commandToClient(%client,'BC_getMinigameConfigData_Tick',%this.configFile[%i]);

	//saveFile TRANSFER END
	commandToClient(%client,'BC_getMinigameConfigData_End');
}

// +----------------------------+
// | BuildCycle_RotationHandler |
// +----------------------------+
function BuildCycle_RotationHandler::onAdd(%this)
{
	%this.stopRotation();
}

function BuildCycle_RotationHandler::onRemove(%this)
{
	%this.save("config/server/Build Cycle/buildCycle.cs");
}

function BuildCycle_RotationHandler::sendBuilds(%this,%client)
{
	if(!BuildCycle.canEdit(%client))
		return;

	//BUILD TRANSFER START
	commandToClient(%client,'BC_getBuildData_Start',%this.getCount());

	//BUILD TRANSFER TICK
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%b = %this.getObject(%i);
		commandToClient(%client,'BC_getBuildData_Tick',%b.getID(),%b.saveFile,%b.minigameConfig);
	}

	//BUILD TRANSFER END
	commandToClient(%client,'BC_getBuildData_End');
}

function BuildCycle_RotationHandler::addBuild(%this,%saveFile,%minigameConfig)
{
	if(!isFile(%saveFile) || !isFile(%minigameConfig))
		return;

	//this object used to hold a lot more data..
	%build = new scriptObject()
	{
		saveFile = %saveFile;
		minigameConfig = %minigameConfig;
	};
	%this.add(%build);
	if(isObject(missionCleanup) && missionCleanup.isMember(%build))
		missionCleanup.remove(%build);

	if(%this.playList !$= "")
		%this.playList = setField(%this.playList,getFieldCount(%this.playList),%build);

	return %build;
}

function BuildCycle_RotationHandler::removeBuild(%this,%build)
{
	if(!isObject(%build))
		return;

	for(%i=0; %i < getFieldCount(%this.playList); %i++)
	{
		%f = getField(%this.playList,%i);
		if(%f == %build)
		{
			%this.playList = removeField(%this.playList,%i);
			break;
		}
	}

	// if(%this.currentBuild == %build)
		// %nextBuild = 1;

	%build.delete();

	// if(%nextBuild)
		// %this.nextBuild();
}

function BuildCycle_RotationHandler::startRotation(%this)
{
	if(%this.rotationRunning)
		return;

	%this.rotationRunning = 1;
	%this.playList = "";
	%this.currentBuild = "";
	%this.nextBuild(0);
}

function BuildCycle_RotationHandler::stopRotation(%this)
{
	%this.playList = "";
	%this.currentBuild = "";
	%this.rotationRunning = 0;
}

function BuildCycle_RotationHandler::nextBuild(%this,%time)
{
	if(!%this.rotationRunning)
		return;
	if(%this.getCount() < 1)
		return;
	if(%time $= "")
		%time = 5000;

	if(%this.playList $= "") //generate new playlist
	{
		for(%i=0; %i < %this.getCount(); %i++)
			%this.playList = setField(%this.playList,%i,%this.getObject(%i));
	}

	if(%this.shuffle)
		%r = getRandom(0,getFieldCount(%this.playList)-1);
	else
		%r = 0;

	%next = getField(%this.playList,%r);
	%this.setBuild(%next,%time);
}

function BuildCycle_RotationHandler::setBuild(%this,%build,%time)
{
	if(!%this.rotationRunning)
		return;
	if(!isObject(%build))
	{
		Slayer_Support::Error("BuildCycle_RotationHandler::setBuild","Build object not found");
		return;
	}
	if(%time $= "")
		%time = 5000;

	//remove build from playlist
	for(%i = 0; %i < getFieldCount(%this.playList); %i ++)
	{
		if(%build == getField(%this.playList,%i))
		{
			%this.playList = removeField(%this.playList,%i);
			break; //only remove the first instance
		}
	}

	Slayer_Support::Debug(0,"BuildCycle_RotationHandler::setBuild","Setting build to" SPC %build);
	%this.rotatingBuild = 1;

	//notify players
	if(%this.popupNotification)
		commandToAll('MessageBoxOK',"Build Cycle","Changing build to \"" @ fileBase(%build.saveFile) @ "\".\n\n" @ %this.notificationText);
	messageAll('',"\c5" @ %this.notificationText);

	Slayer.Minigames.getHostMinigame().buildCycle_roundsPassed = 0;

	%this.schedule(%time,preLoadCleanup,%build);
}

function BuildCycle_RotationHandler::preLoadCleanup(%this,%build)
{
	%this.currentBuild = %build;

	Slayer_Support::Debug(0,"BuildCycle_RotationHandler::preLoadCleanup");

	%this.brickGroupCount = mainBrickGroup.getCount();
	for(%i = 0; %i < %this.brickGroupCount; %i ++)
	{
		%bg = mainBrickGroup.getObject(%i);
		%bg.chainDeleteCallback = "BuildCycle_RotationHandler.attemptBrickLoad(" @ %build @ ");";
		%bg.chainDeleteAll();
	}
}

function BuildCycle_RotationHandler::attemptBrickLoad(%this,%build)
{
	//bricks are still being cleared
	//just wait for the next callback
	if(getBrickCount() > 0)
	{
		Slayer_Support::Debug(0,"BuildCycle_RotationHandler::attemptBrickLoad","Not all bricks have been cleared yet. Waiting...");
		return;
	}

	%path = %build.saveFile;
	if(!isFile(%path))
	{
		Slayer_Support::Error("BuildCycle_RotationHandler::attemptBrickLoad","Unable to find save file," SPC %path);
		return;
	}

	%fileName = %path;
	%colorMethod = 3;
	%fileDirectory = "";//filePath(%path);
	%ownershipMode = %this.loadOwnershipMode;

	Slayer_Support::Debug(0,"BuildCycle_RotationHandler::attemptBrickLoad","Loading bricks. saveFile=" @ %fileName @ "; colorMethod=" @ %colorMethod @ "; fileDirectory=" @ %fileDirectory @ "; ownershipMode=" @ %ownershipMode @ ";");

	serverDirectSaveFileLoad(%fileName,%colorMethod,%fileDirectory,%ownershipMode);
}

// +------------+
// | serverCmds |
// +------------+
function serverCmdBuildCycle(%client,%cmd,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q,%r,%s,%t)
{
	if(%client.isSpamming())
		return;

	switch$(%cmd)
	{
		case "start":
			if(!BuildCycle.canEdit(%client))
				return;
			if(BuildCycle_RotationHandler.rotationRunning)
			{
				messageClient(%client,'',"Build Cycle: The build cycle is already running! Type \c3/buildCycle \crto to view a list of builds.");
				return;
			}

			messageAll('',"Build Cycle: \c3" @ %client.getPlayerName() SPC "\crstarted the build cycle. Type \c3/buildCycle \crto to view a list of builds.");
			BuildCycle_RotationHandler.startRotation();

		case "stop":
			if(!BuildCycle.canEdit(%client))
				return;
			if(!BuildCycle_RotationHandler.rotationRunning)
			{
				messageClient(%client,'',"Build Cycle: The build cycle is not running!");
				return;
			}

			messageAll('',"Build Cycle: \c3" @ %client.getPlayerName() SPC "\crstopped the build cycle.");
			BuildCycle_RotationHandler.stopRotation();

		case "add":
			if(!BuildCycle.canEdit(%client))
				return;

			%concatenated = %a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l SPC %m SPC %n SPC %o SPC %p SPC %q SPC %r SPC %s SPC %t;
			%concatenated = trim(%concatenated);
			%break = strPos(%concatenated,";");
			%length = strLen(%concatenated);
			if(%break > 0 && %length >= %break)
			{
				%saveName = trim(getSubStrR(%concatenated,0,%break)); //getSubStrR is contained in 'LibStr'
				%configName = trim(getSubStrR(%concatenated,%break + 1,%length));
				%saveFile = "saves/" @ %saveName @ ".bls";
				%configFile = $Slayer::Server::ConfigDir @ "/config_saved/config_" @ %configName @ ".cs";

				if(isFile(%saveFile) && isFile(%configFile))
				{
					BuildCycle_RotationHandler.addBuild(%saveFile,%configFile);
					messageAll('',"Build Cycle: \c3" @ %client.getPlayerName() SPC "\cradded\c3" SPC %saveName SPC "\crto the build cycle.");
				}
				else
				{
					messageClient(%client,'',"Build Cycle: Please specify a save file and minigame settings with the following format:");
					messageClient(%client,'',"ADD BUILD FORMAT: \c3/buildCycle add Save File Name; Minigame Settings Name");
				}
			}
			else
			{
				messageClient(%client,'',"Build Cycle: Please specify a save file and minigame settings with the following format:");
				messageClient(%client,'',"ADD BUILD FORMAT: \c3/buildCycle add Save File Name; Minigame Settings Name");
			}

		case "remove":
			if(!BuildCycle.canEdit(%client))
				return;

			if(%a <= BuildCycle_RotationHandler.getCount() && %a > 0 && %a !$= "")
			{
				%build = BuildCycle_RotationHandler.getObject(%a - 1);
				if(!isObject(%build))
					return;
				%saveName = fileBase(%build.saveFile);
				messageAll('',"Build Cycle: \c3" @ %client.getPlayerName() SPC "\crremoved\c3" SPC %saveName SPC "\crfrom the build cycle.");
				BuildCycle_RotationHandler.removeBuild(%build);
			}
			else
			{
				messageClient(%client,'',"Build Cycle: Please enter the build number to remove. Type \c3/buildCycle \crto to view a list of builds.");
				return;
			}

		case "next":
			if(!BuildCycle.canEdit(%client))
				return;
			if(!BuildCycle_RotationHandler.rotationRunning)
			{
				messageClient(%client,'',"Build Cycle: The build cycle has not been started yet. Type \c3/buildCycle Start \crto begin.");
				return;
			}
			if(BuildCycle_RotationHandler.getCount() <= 0)
			{
				messageClient(%client,'',"Build Cycle: The build cycle does not contain any builds. Type \c3/buildCycle Add \crto add builds.");
				return;
			}

			messageAll('',"Build Cycle: \c3" @ %client.getPlayerName() SPC "\crchanged to the next build.");
			BuildCycle_RotationHandler.nextBuild();

		case "goTo":
			if(!BuildCycle.canEdit(%client))
				return;
			if(!BuildCycle_RotationHandler.rotationRunning)
			{
				messageClient(%client,'',"Build Cycle: The build cycle has not been started yet. Type \c3/buildCycle Start \crto begin.");
				return;
			}
			if(BuildCycle_RotationHandler.getCount() <= 0)
			{
				messageClient(%client,'',"Build Cycle: The build cycle does not contain any builds. Type \c3/buildCycle Add \crto add builds.");
				return;
			}

			if(%a <= BuildCycle_RotationHandler.getCount() && %a > 0 && %a !$= "")
			{
				%build = BuildCycle_RotationHandler.getObject(%a - 1);
				if(!isObject(%build))
					return;
				%saveName = fileBase(%build.saveFile);
				messageAll('',"Build Cycle: \c3" @ %client.getPlayerName() SPC "\crchanged the build to\c3" SPC %saveName @ "\cr.");
				BuildCycle_RotationHandler.setBuild(%build);
			}
			else
			{
				messageClient(%client,'',"Build Cycle: Please enter the build number to load. Type \c3/buildCycle \crto to view a list of builds.");
				return;
			}

		case "edit":
			if(!BuildCycle.canEdit(%client))
				return;

			//send data for the GUI
			if(%client.slayerMod_buildCycle)
				BuildCycle.sendData(%client);
			else
				commandToClient(%client,'messageBoxOK',"Build Cycle","\nScript_BuildCycle\n\nYou need Script_BuildCycle in order to use the build cycle GUI.\n\n<a:https://bitbucket.org/Greek2me/script_buildcycle/downloads>Download</a>");

		case "about":
			messageClient(%client,'',"This server is running \c3Build Cycle v" @ $BuildCycle::Server::Version @ "\cr, by Greek2me (Blockland ID 11902).");

		default:
			//list all builds
			messageClient(%client,'',"\c3BUILD CYCLE:");
			for(%i = 0; %i < BuildCycle_RotationHandler.getCount(); %i ++)
			{
				%build = BuildCycle_RotationHandler.getObject(%i);
				messageClient(%client,'',"\c3" SPC %i + 1 @ ". \cr" SPC fileBase(%build.saveFile) @ ";" SPC fileBase(%build.minigameConfig));
			}
	}
}

function serverCmdBC_getBuildData_Start(%client,%buildCount)
{

}

function serverCmdBC_getBuildData_Tick(%client,%build,%saveFile,%minigameConfig)
{
	if(!BuildCycle.canEdit(%client))
		return;
	if(striPos(%saveFile,"saves/") != 0)
		return;
	if(striPos(%minigameConfig,$Slayer::Server::ConfigDir @ "/config_saved/config_") != 0)
		return;

	if(isObject(%build) && BuildCycle_RotationHandler.isMember(%build))
	{
		%build.saveFile = %saveFile;
		%build.minigameConfig = %minigameConfig;
	}
	else
	{
		BuildCycle_RotationHandler.addBuild(%saveFile,%minigameConfig);
	}
}

function serverCmdBC_getBuildData_End(%client,%reSendBuilds)
{
	if(!buildCycle.canEdit(%client))
		return;

	if(%client.isSpamming())
		return;

	if(%reSendBuilds)
		buildCycle_rotationHandler.sendBuilds(%client);
}

function serverCmdBC_removeBuilds(%client,%removeBuildList)
{
	if(!BuildCycle.canEdit(%client))
		return;

	for(%i=0; %i < getFieldCount(%removeBuildList); %i++)
	{
		%f = getField(%removeBuildList,%i);

		if(isObject(%f) && BuildCycle_RotationHandler.isMember(%f))
			BuildCycle_RotationHandler.removeBuild(%f);
	}
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package BuildCycle_server
{
	function Slayer::onClientHandshakeAccepted(%this,%client,%version,%modVersions)
	{
		%parent = parent::onClientHandshakeAccepted(%this,%client,%version,%modVersions);

		if(%client.slayerMod_BuildCycle)
			BuildCycle.sendInitialData(%client);

		return %parent;
	}

	function Slayer_MinigameSO::endRound(%this,%winner,%resetTime)
	{
		%parent = parent::endRound(%this,%winner,%resetTime);

		if(BuildCycle_RotationHandler.rotationRunning && !BuildCycle_RotationHandler.rotatingBuild && BuildCycle_RotationHandler.minigameRoundsPerBuild > 0)
		{
			if(Slayer.Minigames.getHostMinigame() == %this)
			{
				%this.BuildCycle_roundsPassed ++;
				if(%this.BuildCycle_roundsPassed >= BuildCycle_RotationHandler.minigameRoundsPerBuild)
				{
					cancel(%this.resetTimer);

					if(%resetTime $= "")
						%resetTime = %this.timeBetweenRounds * 1000;
					BuildCycle_RotationHandler.nextBuild(%resetTime);
				}
			}
		}

		return %parent;
	}

	function ServerLoadSaveFile_End()
	{
		%parent = parent::ServerLoadSaveFile_End();

		if(BuildCycle_RotationHandler.rotatingBuild)
		{
			%mini = Slayer.Minigames.getHostMinigame();
			if(isObject(%mini))
			{
				%minigameConfig = BuildCycle_RotationHandler.currentBuild.minigameConfig;
				%path = %minigameConfig;
				if(isFile(%path))
				{
					%mini.resetPrefs();
					exec(%path);
				}
				%mini.reset(0);
			}

			BuildCycle_RotationHandler.rotatingBuild = 0;
		}

		return %parent;
	}

	//CLEANUP
	function destroyServer()
	{
		if(isObject(BuildCycle))
			BuildCycle.delete();

		return parent::destroyServer();
	}

	function onExit()
	{
		if(isObject(BuildCycle))
			BuildCycle.delete();

		return parent::onExit();
	}
};
activatePackage(BuildCycle_server);

//REGISTER PREFERENCES
if(!$Slayer::Server::Dependencies::Preferences)
	exec("Add-Ons/Gamemode_Slayer/Dependencies/Preferences.cs");

//all prefs are host-only

//we are using "transient prefs"
//because we have our own saving mechanism
//we just do this for GUI access
Slayer.Prefs.addTransientPref("Build Cycle","Display Popup Notification","BuildCycle_RotationHandler.popupNotification","bool",1,0,0,0,"Advanced");
Slayer.Prefs.addTransientPref("Build Cycle","Notification","BuildCycle_RotationHandler.notificationText","string 150","The server is changing to a new build. The game will resume after the build is changed.\n\nPlease wait...",0,0,0,"Advanced");
Slayer.Prefs.addTransientPref("Build Cycle","Shuffle","BuildCycle_RotationHandler.shuffle","bool",0,0,1,0);
Slayer.Prefs.addTransientPref("Build Cycle","Minigame Rounds Per Build","BuildCycle_RotationHandler.minigameRoundsPerBuild","int 0 500",0,0,1,0,"Advanced");
Slayer.Prefs.addTransientPref("Build Cycle","Load Bricks As","BuildCycle_RotationHandler.loadOwnershipMode","list" TAB "0 Host" TAB "1 Saved Owner" TAB "2 Public",0,0,0,0,"Advanced");

//INITIALIZE BuildCycle
if(!isObject(BuildCycle))
{
	$BuildCycle::Server = new scriptGroup(BuildCycle);
	if(isObject(missionCleanup) && missionCleanup.isMember(BuildCycle))
		missionCleanup.remove(BuildCycle);
}