// +--------------------------------+
// |  _  _   _   _   _      _     _ |
// | | \/ | |_| |_| |  |_| |  |  |_ |
// | |    | | | |   |_  |  |_ |_ |_ |
// |  Greek2me | Blockland ID 11902 |
// +-----------------------------+--+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

if(!isFile("Add-ons/Gamemode_Slayer/client.cs"))
{
	error("ERROR: Script_BuildCycle - Required add-on Gamemode_Slayer not found!");
	return;
}

$BuildCycle::Client::Version = "1.0 Alpha 1";

// +------------------+
// | BuildCycleClient |
// +------------------+
function BuildCycleClient::onAdd(%this)
{
	if(!isObject(buildCycleClient_saveFileHandler))
	{
		$BuildCycle::Client::saveFileHandler = new scriptGroup(buildCycleClient_saveFileHandler)
		{
			numSaves = 0;
		};
		if(isObject(missionCleanup) && missionCleanup.isMember(buildCycleClient_saveFileHandler))
			missionCleanup.remove(buildCycleClient_saveFileHandler);
		%this.add(buildCycleClient_saveFileHandler);
	}

	if(!isObject(buildCycleClient_minigameConfigFileHandler))
	{
		$BuildCycle::Client::minigameConfigFileHandler = new scriptGroup(buildCycleClient_minigameConfigFileHandler)
		{
			numConfigFiles = 0;
		};
		if(isObject(missionCleanup) && missionCleanup.isMember(buildCycleClient_minigameConfigFileHandler))
			missionCleanup.remove(buildCycleClient_minigameConfigFileHandler);
		%this.add(buildCycleClient_minigameConfigFileHandler);
	}

	if(!isObject(buildCycleClient_RotationHandler))
	{
		$BuildCycle::Client::RotationHandler = new scriptGroup(buildCycleClient_RotationHandler);
		if(isObject(missionCleanup) && missionCleanup.isMember(buildCycleClient_RotationHandler))
			missionCleanup.remove(buildCycleClient_RotationHandler);
		%this.add(buildCycleClient_RotationHandler);
	}

	exec("./GUI/buildCycle_Main.gui");
	exec("./GUI/scripts.cs");
}

function BuildCycleClient::onDisconect(%this)
{
	SlayerClient_Support::Debug(0,"Build Cycle","Disconnecting from server");

	Slayer_Main_Tabs.removeTab("Build Cycle");

	%this.serverHasBuildCycle = 0;
}

function BuildCycleClient::clearAllData(%this)
{
	SlayerClient_Support::Debug(0,"Clearing data","Build Cycle");

	buildCycleClient_saveFileHandler.clearSaveData();
	buildCycleClient_minigameConfigFileHandler.clearConfigData();
	buildCycleClient_RotationHandler.clearBuilds();
}

function BuildCycleClient::sendBuild(%this,%build)
{
	//this server-client system was intented to be much more complicated...
	// the client would hold onto everthing until they were done editing
	// and then send everything at once
	//That never happened.

	commandToServer('BC_getBuildData_Start',1);
	commandToServer('BC_getBuildData_Tick',%build.id,%build.saveFile,%build.minigameConfig);
	commandToServer('BC_getBuildData_End',1);
}

function BuildCycleClient::sendPrefs(%this)
{
	for(%i = 0; %i < SlayerClient.serverPrefs.getCount(); %i ++)
	{
		%p = SlayerClient.serverPrefs.getObject(%i);
		if(%p.category !$= "Build Cycle")
			continue;
		%val = %p.getValue();
		if(%val !$= %pref.origValue)
		{
			SlayerClient_Support::Debug(2,"BuildCycle: Sending pref",%p.category TAB %p.title TAB %val);
			commandToServer('Slayer_getPrefs_Tick',%p.category,%p.title,%val);
		}
	}
}

// +----------------------------------+
// | buildCycleClient_saveFileHandler |
// +----------------------------------+
function buildCycleClient_saveFileHandler::addSaveData(%this,%saveFile)
{
	%this.saveFile[%this.numSaves] = %saveFile;
	BuildCycle_Main_SaveList.add(fileBase(%saveFile),%this.numSaves);
	%this.numSaves ++;
}

function buildCycleClient_saveFileHandler::getSaveDataIndex(%this,%saveFile)
{
	for(%i = 0; %i < %this.numSaves; %i ++)
	{
		%s = %this.saveFile[%i];
		if(%s $= %saveFile)
			return %i;
	}
	return -1;
}

function buildCycleClient_saveFileHandler::clearSaveData(%this)
{
	for(%i=0; %i < %this.numSaves; %i++)
		%this.saveFile[%i] = "";
	%this.numSaves = 0;

	BuildCycle_Main_SaveList.clear();
}

// +----------------------------------+
// | buildCycleClient_saveFileHandler |
// +----------------------------------+
function buildCycleClient_minigameConfigFileHandler::addConfigData(%this,%configFile)
{
	%this.configFile[%this.numConfigFiles] = %configFile;
	%name = restWords(strReplace(fileBase(%configFile),"_"," "));
	BuildCycle_Main_MinigameConfigList.add(%name,%this.numConfigFiles);
	%this.numConfigFiles ++;
}

function buildCycleClient_minigameConfigFileHandler::getConfigDataIndex(%this,%configFile)
{
	for(%i = 0; %i < %this.numConfigFiles; %i ++)
	{
		%s = %this.configFile[%i];
		if(%s $= %configFile)
			return %i;
	}
	return -1;
}

function buildCycleClient_minigameConfigFileHandler::getConfigDataIndexFromName(%this,%configName)
{
	for(%i = 0; %i < %this.numConfigFiles; %i ++)
	{
		%file = %this.configFile[%i];
		%name = restWords(strReplace(fileBase(%file),"_"," "));
		if(%name $= %configName)
			return %i;
	}
	return -1;
}

function buildCycleClient_minigameConfigFileHandler::clearConfigData(%this)
{
	for(%i=0; %i < %this.numConfigFiles; %i++)
		%this.configFile[%i] = "";
	%this.numConfigFiles = 0;

	BuildCycle_Main_MinigameConfigList.clear();
}

// +----------------------------------+
// | buildCycleClient_RotationHandler |
// +----------------------------------+
function buildCycleClient_RotationHandler::addBuild(%this,%id,%saveFile,%minigameConfig)
{
	%build = new scriptObject()
	{
		id = %id;
		saveFile = %saveFile;
		minigameConfig = %minigameConfig;
	};
	%this.add(%build);
	if(isObject(missionCleanup) && missionCleanup.isMember(%build))
		missionCleanup.remove(%build);

	BuildCycle_Main_buildList.addRow(%build,fileBase(%saveFile));

	return %build;
}

function buildCycleClient_RotationHandler::removeBuild(%this,%build)
{
	BuildCycle_Main_buildList.removeRowByID(%build);
	%build.delete();
}

function buildCycleClient_RotationHandler::getBuildFromID(%this,%id)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%b = %this.getObject(%i);
		if(%b.id == %id)
			return %b;
	}
	return -1;
}

function buildCycleClient_RotationHandler::sendBuilds(%this)
{
	//MAP TRANSFER START
	commandToServer('BC_getBuildData_Start',%this.getCount());

	//MAP TRANSFER TICK
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%b = %this.getObject(%i);
		commandToServer('BC_getBuildData_Tick',%b.id,%b.saveFile,%b.minigameConfig);
	}

	//MAP TRANSFER END
	commandToServer('BC_getBuildData_End');
}

function buildCycleClient_RotationHandler::clearBuilds(%this)
{
	%this.deleteAll();
	BuildCycle_Main_buildList.clear();
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdBC_getInitialData_Start()
{
	BuildCycleClient.clearAllData();

	BuildCycleClient.serverHasBuildCycle = 1;

	Slayer_Main_Tabs.addTab("Build Cycle","BuildCycle_Main",0,0,"Add-Ons/Script_BuildCycle/Images/map.png");
}

function clientCmdBC_getInitialData_End()
{

}

function clientCmdBC_getData_Start()
{

}

function clientCmdBC_getData_End(%gui)
{
	if(isObject(%gui))
	{
		if(%gui.isAwake())
			%gui.refreshAllValues();
		else
			canvas.pushDialog(%gui);
	}
}

function clientCmdBC_getSaveData_Start()
{
	buildCycleClient_saveFileHandler.clearSaveData();
}

function clientCmdBC_getSaveData_Tick(%saveFile)
{
	buildCycleClient_saveFileHandler.addSaveData(%saveFile);
}

function clientCmdBC_getSaveData_End()
{

}

function clientCmdBC_getMinigameConfigData_Start()
{
	buildCycleClient_minigameConfigFileHandler.clearConfigData();
}

function clientCmdBC_getMinigameConfigData_Tick(%configFile)
{
	buildCycleClient_minigameConfigFileHandler.addConfigData(%configFile);
}

function clientCmdBC_getMinigameConfigData_End()
{

}

function clientCmdBC_getBuildData_Start(%buildCount)
{
	buildCycleClient_RotationHandler.clearBuilds();
}

function clientCmdBC_getBuildData_Tick(%id,%saveFile,%minigameConfig)
{
	buildCycleClient_RotationHandler.addBuild(%id,%saveFile,%minigameConfig);
}

function clientCmdBC_getBuildData_End()
{

}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package BuildCycle_Client
{
	function SlayerClient::onAdd(%this)
	{
		parent::onAdd(%this);

		if(!$BuildCycle::Client::registeredVersion)
		{
			$Slayer::Client::ModVersions = setField
			(
				$Slayer::Client::ModVersions,
				getFieldCount($Slayer::Client::ModVersions),
				"BuildCycle" SPC $BuildCycle::Client::Version
			);

			$BuildCycle::Client::registeredVersion = 1;
		}

		//INITIALIZE BuildCycle
		if(!isObject(BuildCycleClient))
		{
			$BuildCycle::Client = new scriptGroup(BuildCycleClient);
			if(isObject(missionCleanup) && missionCleanup.isMember(BuildCycleClient))
				missionCleanup.remove(BuildCycleClient);
		}

		// +----------+
		// | Keybinds |
		// +----------+
		SlayerClient_Support::addKeyBind("Build Cycle","Edit Build Cycle","buildCycleClient_pushMain");
		SlayerClient_Support::addKeyBind("Build Cycle","Next Build","buildCycleClient_nextBuild");
	}

	function disconnect(%a)
	{
		%parent = parent::disconnect(%a);

		BuildCycleClient.onDisconnect();

		return %parent;
	}
};
activatePackage(BuildCycle_Client);