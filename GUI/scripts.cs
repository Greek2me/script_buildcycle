function buildCycleClient_pushMain(%flag)
{
	if(%flag)
		canvas.pushDialog(buildCycle_Main);
}

function buildCycle_Main::onWake(%this)
{
	%this.cancel();
	commandToServer('buildCycle',"edit");
}

function buildCycle_Main::clickHelp(%this,%help)
{
	switch$(%help)
	{
		case "minigameSettings": %text = "Sorry, help is not available yet.\n\n                    :(";
		case "roundsBetweenRotation": %text = "After the minigame goes through this many rounds, the build will be changed.\n\nIt is recommended that you include Victory Conditions in your minigames so that rounds can end.";
		default: %text = "Sorry, help text is not available.";
	}

	setPopup(%this,1,"OK","help","Build Cycle | Help",%text);
}

function buildCycle_Main_buildList::onSelect(%this,%build)
{
	if(!isObject(%build))
		return;

	buildCycle_Main_SaveList.setSelected(buildCycleClient_saveFileHandler.getSaveDataIndex(%build.saveFile));
	buildCycle_Main_MinigameConfigList.setSelected(buildCycleClient_minigameConfigFileHandler.getConfigDataIndex(%build.minigameConfig));

	buildCycle_Main_Edit.setVisible(1);
}

function buildCycle_Main::rotation_start(%this)
{
	commandToServer('buildCycle',"start");
}

function buildCycle_Main::rotation_stop(%this)
{
	commandToServer('buildCycle',"stop");
}

function buildCycle_Main::rotation_next(%this)
{
	commandToServer('buildCycle',"next");
}

function buildCycle_Main::rotation_goTo(%this)
{
	%build = BuildCycle_Main_buildList.getSelectedID();
	if(isObject(%build))
		commandToServer('buildCycle',"goTo",%build.getGroupID() + 1);
	else
		setPopup(buildCycle_Main,0,"OK","exclamation","Oops!","Please select a build to go to.");
}

function buildCycle_Main::addBuild(%this)
{
	%this.cancel();

	buildCycle_Main_SaveList.setSelected(0);
	buildCycle_Main_MinigameConfigList.setSelected(0);

	buildCycle_Main_Edit.setVisible(1);
}

function buildCycle_Main::removeBuild(%this,%build)
{
	if(!isObject(%build))
		return;

	%this.cancel();

	commandToServer('BC_removeBuilds',%build.id);
	buildCycleClient_RotationHandler.removeBuild(%build);
}

function buildCycle_Main::Apply(%this,%build)
{
	%saveFile = buildCycleClient_saveFileHandler.saveFile[buildCycle_Main_SaveList.getSelected()];
	%minigameConfig = buildCycleClient_minigameConfigFileHandler.configFile[buildCycle_Main_MinigameConfigList.getSelected()];

	if(isObject(%build))
	{
		%build.saveFile = %saveFile;
		%build.minigameConfig = %minigameConfig;
	}
	else
	{
		%build = buildCycleClient_rotationHandler.addBuild(-1,%saveFile,%minigameConfig);
	}

	buildCycleClient.sendBuild(%build);

	%this.cancel();
}

function buildCycle_Main::cancel(%this)
{
	buildCycle_Main_buildList.clearSelection();
	buildCycle_Main_buildPreview.setBitmap("Add-Ons/Script_BuildCycle/Images/unknownBuild.png");
	buildCycle_Main_Edit.setVisible(0);
}

function buildCycle_Main::applyPrefs(%this)
{
	BuildCycleClient.sendPrefs();
}

function buildCycle_Main_SaveList::onSelect(%this,%id)
{
	%saveFile = buildCycleClient_saveFileHandler.saveFile[%id];
	%partialPath = filePath(%saveFile) @ "/" @ fileBase(%saveFile);

	if(isFile(%partialPath @ ".jpg") || isFile(%partialPath @ ".png"))
		%path = %partialPath;
	else
		%path = "Add-Ons/Script_BuildCycle/Images/unknownBuild.png";

	buildCycle_Main_buildPreview.setBitmap(%path);
}